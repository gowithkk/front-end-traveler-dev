import React from 'react';
import classes from './Bob.module.css';

const Bob = () => {
    return (
        <>
            <div className={classes.title}>
                <h1>Hello, I am Bob</h1>
                <h1>This is the second push</h1>
            </div>
        </>
    )
};

export default Bob;
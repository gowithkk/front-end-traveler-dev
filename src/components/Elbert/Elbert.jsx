import React from 'react';
import classes from './Elbert.module.css';

const Elbert = () => {
    return (
        <>
            <div className={classes.title}>
                <h1>Hello, I am Elbert</h1>
                <h1>This is the second push</h1>
            </div>
        </>
    )
};

export default Elbert;